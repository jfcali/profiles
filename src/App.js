import React from 'react';
import Profile from './pages/Profile/Profile';
import Profiles from './pages/Profiles/Profiles';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter forceRefresh>
      <Switch>
        <Route path="/profiles/:page">
          <Profiles />
        </Route>
        <Route path="/profile/:id">
          <Profile />
        </Route>
        <Route path="/">
          <Redirect to="/profiles/1" />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
