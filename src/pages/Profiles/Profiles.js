import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import Search from '../components/Search/Search';
import List from '../components/List/List';
import { useSelector, useDispatch } from 'react-redux';
import { getAsyncCharacters, filterCharacters } from '../../state/slices/sliceProfiles';
import styles from './Profiles.module.css';
import Button from '@mui/material/Button';

const Profiles = () => {
  const params = useParams();
  let history = useHistory();
  const dispatch = useDispatch();
  const { profiles, next, prev, totalPages, loading } = useSelector((state) => state.profiles);

  useEffect(() => {
    dispatch(getAsyncCharacters({ page: params.page }));
  }, []);

  // To-do: implmenet filters as url queries
  // const composeFilters = () => {
  //   const keys = Object.keys(search);
  //   return keys.reduce((filter, current) => {
  //     const value = search[current];
  //     let ext = '';
  //     if (value) {
  //       if (filter.startsWith('?')) {
  //         ext += '&';
  //       } else {
  //         ext += '?';
  //       }

  //       ext += `${current}=${value}`;
  //     }
  //     return filter + ext;
  //   }, '');
  // };

  const nextPage = () => {
    dispatch(getAsyncCharacters({ page: next }));
    history.push(`/profiles/${next}`);
  };

  const prevPage = () => {
    dispatch(getAsyncCharacters({ page: prev }));
    history.push(`/profiles/${prev}`);
  };

  const searchHandler = (newFilters) => {
    dispatch(filterCharacters({ page: params.page, filters: newFilters }));
  };

  return (
    <main>
      <Search searchHandler={searchHandler} />
      <List rows={profiles} />
      <footer className={styles.footer}>
        {loading ? (
          'loading'
        ) : (
          <>
            <Button disabled={!prev} onClick={prevPage}>
              prev
            </Button>
            {`${params.page}/${totalPages}`}
            <Button disabled={!next} onClick={nextPage}>
              next
            </Button>
          </>
        )}
      </footer>
    </main>
  );
};

export default Profiles;
