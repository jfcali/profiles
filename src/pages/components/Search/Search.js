import React, { useState } from 'react';
import styles from './Search.module.scss';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';

const Search = (props) => {
  const [filters, setFilters] = useState({ name: '', status: '', type: '' });

  const updateName = (e) => {
    setFilters((prevFilters) => {
      return { ...prevFilters, name: e.target.value };
    });
  };

  const updateStatus = (e) => {
    setFilters((prevFilters) => {
      return { ...prevFilters, status: e.target.value };
    });
  };

  const updateType = (e) => {
    setFilters((prevFilters) => {
      return { ...prevFilters, type: e.target.value };
    });
  };

  return (
    <Grid container spacing={4} className={styles.Search}>
      <Grid item xs={12} md={3}>
        <TextField
          id="standard-search"
          label="Search by name"
          type="search"
          variant="standard"
          fullWidth
          onChange={updateName}
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <TextField
          id="standard-search"
          label="Search by type"
          type="search"
          variant="standard"
          fullWidth
          onChange={updateType}
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          label="Status"
          fullWidth
          value={filters.status}
          onChange={updateStatus}
        >
          <MenuItem value="">Any</MenuItem>
          <MenuItem value="alive">Alive</MenuItem>
          <MenuItem value="dead">Dead</MenuItem>
          <MenuItem value="unknown">Unknown</MenuItem>
        </Select>
      </Grid>
      <Grid item xs={12} md={3}>
        <Button variant="contained" onClick={() => props.searchHandler(filters)}>
          Filter
        </Button>
      </Grid>
    </Grid>
  );
};

export default Search;
