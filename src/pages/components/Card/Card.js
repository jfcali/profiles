import React from 'react';
import PropTypes from 'prop-types';
import styles from './Card.module.scss';
import { IconButton } from '@mui/material';
import CloseSharpIcon from '@mui/icons-material/CloseSharp';

const Card = (props) => {
  const close = () => {
    if (props.onClose) props.onClose();
  };
  return (
    <article className={styles.Card}>
      <IconButton onClick={close} className={styles.Card__close}>
        <CloseSharpIcon fontSize="large" />
      </IconButton>
      {props.children}
    </article>
  );
};

Card.propTypes = {
  title: PropTypes.string,
};

export default Card;
