import React from 'react';
import PropTypes from 'prop-types';
import styles from './List.module.scss';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import { useHistory } from 'react-router-dom';

const List = (props) => {
  let history = useHistory();
  return (
    <TableContainer className={styles.List}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Type</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.rows.map((row) => (
            <TableRow key={row.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              <TableCell component="th" scope="row" style={{ display: 'flex' }}>
                <Button onClick={() => history.push(`/profile/${row.id}`)}>
                  <img src={row.image} style={{ width: '20px', borderRadius: '50%', marginRight: '5px' }} />
                  {row.name}
                </Button>
              </TableCell>
              <TableCell>{row.type || '-'}</TableCell>
              <TableCell>{row.status}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

List.propTypes = {
  rows: PropTypes.array.isRequired,
};

export default List;
