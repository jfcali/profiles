import React, { useEffect } from 'react';
import Card from '../components/Card/Card';
import styles from './Profile.module.scss';
import { useSelector, useDispatch } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';

import { getCharacter } from '../../state/slices/sliceProfiles';

const Profile = () => {
  const { id } = useParams();
  let history = useHistory();
  const dispatch = useDispatch();
  const { selectedCharacter } = useSelector((state) => state.profiles);

  useEffect(() => {
    dispatch(getCharacter({ id }));
  }, []);

  return (
    <main className={styles.Profile}>
      {selectedCharacter && (
        <Card onClose={() => history.push(`/profiles/1`)}>
          <section className={styles.Profile__card_content}>
            <img className={styles.Profile__image} src={selectedCharacter.image} />
            <h1 className={styles.Profile__title}>{selectedCharacter.name}</h1>
            <p className={styles.Profile__text}>
              {selectedCharacter.type || <span style={{ fontStyle: 'italic' }}>Unknown</span>}
            </p>
            <p className={styles.Profile__text}>{selectedCharacter.status}</p>
          </section>
        </Card>
      )}
    </main>
  );
};

export default Profile;
