import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
  isLoading: false,
  page: 0,
  totalPages: 0,
  next: null,
  prev: null,
  search: { name: '', status: '', type: '' },
  profiles: [],
  selectedCharacter: null,
};

export const getCharacter = createAsyncThunk('getCharacter', ({ id }) => {
  return fetch('https://rickandmortyapi.com/graphql', {
    method: 'POST',

    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `
        query {
          character(id: ${id}) {
            id
            image
            name
            type
            status
          }
        }
      `,
    }),
  }).then((res) => res.json());
});

export const filterCharacters = createAsyncThunk('getProfiles', ({ page, filters }) => {
  const { name, status, type } = filters;
  const query = `
  query {
    characters(page: ${page}, filter: {name: "${name}", status: "${status}", type: "${type}"}) {
      info {  
        pages
        next
        prev
      }
      results {
        id
        image
        name
        type
        status
      }
    }
  }
`;

  return fetch('https://rickandmortyapi.com/graphql', {
    method: 'POST',

    headers: {
      'Content-Type': 'application/json',
    },

    body: JSON.stringify({
      query,
    }),
  }).then((res) => res.json());
});

export const getAsyncCharacters = createAsyncThunk('getProfiles', ({ page }) => {
  return fetch('https://rickandmortyapi.com/graphql', {
    method: 'POST',

    headers: {
      'Content-Type': 'application/json',
    },

    body: JSON.stringify({
      query: `
        query {
          characters(page: ${page}) {
            info {  
              pages
              next
              prev
            }
            results {
              id
              image
              name
              type
              status
            }
          }
        }
      `,
    }),
  }).then((res) => res.json());
});

const slice = createSlice({
  name: 'profiles',
  initialState,
  reducers: {
    search: (state, payload) => {
      state.search = payload.search;
    },
  },
  extraReducers: {
    [getAsyncCharacters.pending]: (state) => {
      state.isLoading = true;
    },
    [getAsyncCharacters.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.profiles = action.payload.data.characters.results;
      state.next = action.payload.data.characters.info.next;
      state.prev = action.payload.data.characters.info.prev;
      state.totalPages = action.payload.data.characters.info.pages;
      state.search = null;
    },
    [getAsyncCharacters.rejected]: (state) => {
      state.isLoading = false;
    },
    [getCharacter.pending]: (state) => {
      state.isLoading = true;
    },
    [getCharacter.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.selectedCharacter = action.payload.data.character;
    },
    [getCharacter.rejected]: (state) => {
      state.isLoading = false;
    },
    [filterCharacters.pending]: (state) => {
      state.isLoading = true;
    },
    [filterCharacters.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.profiles = action.payload.data.characters.results;
      state.next = action.payload.data.characters.info.next;
      state.prev = action.payload.data.characters.info.prev;
      state.totalPages = action.payload.data.characters.info.pages;
      state.search = action.meta.arg.filters;
    },
    [filterCharacters.rejected]: (state) => {
      state.isLoading = false;
    },
  },
});

export const { getProfiles } = slice.actions;

export default slice.reducer;
