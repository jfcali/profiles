import { configureStore } from '@reduxjs/toolkit';
import sliceReducer from './slices/sliceProfiles';

const store = configureStore({
  reducer: {
    profiles: sliceReducer,
  },
});

export default store;
