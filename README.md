# Important:

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Tested with Node v14.19.0.

# Docker Image

This project contains a Dockerfile which can be used to run a production-like image.

## To build the image:

`docker build -t profiles:latest .`

By default, the image will pull from [main](https://gitlab.com/jfcali/profiles). To build a specific branch you can pass an additional build argument:

`docker build --build-arg branch=my_branch_name -t profiles:latest .`

## To run the image:

`docker run -d -p 3333:80 profiles:latest`

Open [http://localhost:3333](http://localhost:3333) to view it in your browser.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

### Router

Users can use 'profiles/${page}' in the url to fast travel to a specific page. They can also use 'profile/${id}' to go directly to the character who matches the id.
