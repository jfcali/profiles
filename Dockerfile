FROM node:14-alpine as builder
RUN apk --no-cache add git
ARG branch=main
RUN echo "Cloning branch ${branch} from 'profiles'" && \
    git clone -b $branch https://gitlab.com/jfcali/profiles && \
    cd profiles && \
    npm install && \
    npm run build

FROM nginx:1.21.0-alpine as production
ENV NODE_ENV production
COPY --from=builder profiles/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
